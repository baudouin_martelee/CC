.. Crowd Couting for UCL documentation master file, created by
   sphinx-quickstart on Fri Apr 24 13:52:52 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Crowd Couting for UCL's documentation!
=================================================

.. toctree::
   :maxdepth: 2
   :caption: Presentation :

   Introduction
   
   Installation

.. toctree::
   :maxdepth: 2
   :caption: Functions :

   Main functions

   Secondary functions

   More Informations
   

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
