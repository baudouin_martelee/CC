Secondary functions
=====================

This part defines the functions use in the main functions

Image module
--------------------------

.. automodule:: Image
   	:members: 
   	:exclude-members: getPaths


GaussianFilter module
--------------------------

The code included in this module is borrowed from https://github.com/davideverona/deep-crowd-counting_crowdnet

.. automodule:: GaussianFilter
   	:members: 


dataset module
--------------------------

.. automodule:: dataset
   	:members: 
   	:special-members: __init__, __len__, __getitem__

train module
--------------------------

.. automodule:: train
   	:members:
   	:exclude-members: pytorch_loader, split_data, train, generate_density_map, main

model module
--------------------------

.. automodule:: model
   	:members:
   	:special-members: __init__


Visualisation module
--------------------------

.. automodule:: Visualisation
   	:members:



