Introduction
=====================

This package was create to count the number of people in an image of crowd.

This documentation is splitted in several parts : 

	1) Installation
	2) Main functions
		This part is composed of all the functions that are part of the package. 
		These function allows you to import data, train data and predict the number of people of an image
	3) Secondary functions
		This part is all the functions called by the main functions. To understand all steps of the process, it is interesting to explore them.