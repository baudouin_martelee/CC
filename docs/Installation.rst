Installation
=====================

First step : cloning the repo
-----------------------------

You have to clone the repo first to have acces to the package

.. code-block:: python

		!git clone https://github.com/baudouinMartelee/CC.git


Second step : install package
-----------------------------

If you install this package for the first time, you should use this line of code : 

.. code-block:: python

		!pip install git+https://github.com/baudouinMartelee/CC.git

If you want to upgrade your package due to an update , you should use this line of code : 

.. code-block:: python

		!pip install git+https://github.com/baudouinMartelee/CC.git --upgrade