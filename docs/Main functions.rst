Main functions
=====================

This part is composed of all the functions that we need to create prediction 


Importation module
--------------------------------

The first part is to import data into from a json file into our IDE

.. automodule:: Importation
    :members:

Image module
--------------------------

.. automodule:: Image
   	:members: 
   	:exclude-members: generate_density, load_dict_img_path_gt, load_image_with_groundtruth

train module
--------------------------

.. automodule:: train
    :members: 
    :exclude-members: main, save_checkpoint

CaptureVideo module
--------------------------

.. automodule:: CaptureVideo
    :members: 