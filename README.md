# Crowd counting

#Clone this git
### git clone 

This git is split in different parts.

1) A package CC 

	this package contains :
	a) the differents python files
	b) a json file that contains a list of images name with the associated groundtruth

2) a package doc for the Sphinx documentation

	This package is split in differents part

	1) a conf.py file to initialise the Sphinx builder
	2) differents .rst file  for settings the elements of modules in a html page
	3) a Makefile to produce the html file
	4) a _build folder that contains 2 folders
		the essential folder to run the documentation is html who you can run documentation by open index.html 

# To access documentation of this git 
    
    Open index.html

    Path :   docs/_build/html/index.html
