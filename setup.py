
from setuptools import setup

setup(
    name='CrowdCountingStage',
    version='0.0.1',
    description='My private package from private github repo',
    url='git@github.com:baudouinMartelee/CC.git',
    author='Baudouin Martelee',
    author_email='',
    license='unlicense',
    packages=['CC'],
    zip_safe=False
)
