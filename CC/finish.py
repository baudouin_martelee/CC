
import matplotlib.pyplot as plt
import numpy as np
import os
import scipy.io as sio
import torch 
import h5py
import sys
import PIL.Image as Image
import torchvision.transforms.functional as F
import PIL
from CC.model import CSRNet
from CC.Importation import import_data
from CC.Image import generate_density
from torchvision import transforms

    
def load_checkpoint(checkpointPath):
    """
    
    Allow to recreate a model from a pth file (checkpoint file)

    Parameters
    ----------
    checkpointPath : Path
        DESCRIPTION.

    Raises
    ------
    
        Path of the pth file that contains the weights and biais of the model

    Returns
    -------
    model : CSRNet
        model 

    """
    
    if os.path.isfile(checkpointPath):
        checkpoint = torch.load(checkpointPath)
        model = checkpoint['model']
        model.load_state_dict(checkpoint['state_dict'])
        
        model.eval()
    else:
        raise  AttributeError("Chemin du checkpoint incorrect")

    return model

def predict_img_test(path, checkpointPath, json_path):
    """
    Predict an image of the test set
    
    Parameters
    ----------
    path : Path
        Path of the image that you want to predict the number of people
    checkpointPath : Path
        Path of the pth file that contains the weights and biais of the model
    json_path : Path
        Path of the json to have 

    Raises
    ------
    AttributeError
        If the pth file is not found

    Returns
    -------
    None.

    """
    
    if not os.path.isfile(path):
        raise AttributeError("Incorrect path of an image")
    
    
    if not os.path.isfile(checkpointPath):
        raise AttributeError("Incorrect path of the pth file (checkpoint)")
    
    if not os.path.isfile(json_path):
        raise AttributeError("Incorrect json path")
    
    
    print("Model evaluation")

    #test
    #audit_path = 'C:/Users/Baudouin/Documents/IPL_BLOC_3/STAGE/Croix du Sud/'


    json_file = open(json_path)
    
    dico_img_gt = import_data(json_file)
    
    
    h5_path = path.replace('jpg','h5')
    if not os.path.exists(h5_path):
        generate_density(path, dico_img_gt)

    print(path)
    
    #img = F.to_tensor(Image.open(path).convert('RGB'))
    img = Image.open(path).convert('RGB')
    #img.show()

    img = F.to_tensor(img)
    
    normalizeImage = transforms.Normalize(mean=[0.5, 0.5, 0.5],
                                     std=[0.5, 0.5, 
                                     0.5])

    img = normalizeImage(img)
    
    model = load_checkpoint(checkpointPath)
    print("model loaded")

    #img = img.cuda()
    output = model(img.unsqueeze(0))
    
    predicted_count = int(output.detach().cpu().sum().numpy())
    print("Predicted Count : ",predicted_count)
    temp = np.asarray(output.detach().cpu().reshape(output.detach().cpu().shape[2],output.detach().cpu().shape[3]))
    plt.imshow(temp,cmap ='jet')
    plt.show()


    temp = h5py.File(path.replace('.jpg','.h5'), 'r')
    temp_1 = np.asarray(temp['density'])

    plt.imshow(temp_1,cmap ='jet')
    count_original = int(np.sum(temp_1)) + 1
    print("Original Count : ",count_original)
    plt.show()
    
    print("Original Image")
    plt.imshow(plt.imread(path))
    plt.show()

    

def predict(path, checkpointPath):
    """
    Predict people's number of an image 

    Parameters
    ----------
    path : Path
        Path of the image that you want to predict the number of people
    checkpointPath : Path
        Path of the pth file that contains the weights and biais of the model
    

    Raises
    ------
    AttributeError
        If the pth file is not found

    Returns
    -------
    None.

    """
    
    if not os.path.isfile(path):
        raise AttributeError("Incorrect path of an image")
    
    
    if not os.path.isfile(checkpointPath):
        raise AttributeError("Incorrect path of the pth file (checkpoint)")

    print("Model evaluation")

    #test
    #audit_path = 'C:/Users/Baudouin/Documents/IPL_BLOC_3/STAGE/Croix du Sud/'


    print(path)
    
    #img = F.to_tensor(Image.open(path).convert('RGB'))
    img = Image.open(path).convert('RGB')
    #img.show()

    img = F.to_tensor(img)
    
    normalizeImage = transforms.Normalize(mean=[0.5, 0.5, 0.5],
                                     std=[0.5, 0.5, 
                                     0.5])

    img = normalizeImage(img)
    
    model = load_checkpoint(checkpointPath)
    print("model loaded")

    #img = img.cuda()
    output = model(img.unsqueeze(0))
    
    predicted_count = int(output.detach().cpu().sum().numpy())
    print("Predicted Count : ",predicted_count)
    temp = np.asarray(output.detach().cpu().reshape(output.detach().cpu().shape[2],output.detach().cpu().shape[3]))
    plt.imshow(temp,cmap ='jet')
    plt.show()




if __name__ == "__main__":

    path = sys.argv[1]
    checkpointPath = sys.argv[2]
    json_path = sys.argv[3]

    predict_img_test(path,checkpointPath,json_path)
