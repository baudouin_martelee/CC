# -*- coding: utf-8 -*-
"""
Created on Fri Mar 13 13:53:51 2020

@author: Baudouin


We have to import data from a certain type of data. Here, data are stored in a json file.
I used the "dataset_audit.json" file. This file contains a lot of keys but we just need 2 information : 
the image name and the groundtruth in form of coordinates x,y of heads.

in my json file, the image name is given by "External ID" 
and the grountruth is in "Label" -> "head" -> "geometry" -> "x" and "y"


In the future version, a recursive method to explore json file will be developped to make this method generic.
"""

import json
import pandas as pd



    
    
def import_data(json_file):
    """
    this method provide a dictionary with the name of an image and the groundtruth associate.
    This method must be change if your json file don't have the same disposition.
    
    
    Parameters
    ----------
     json_file : JSON
         take a json file with the coordinates of head of people
    

    Returns
    -------
    Return : Dictionary
        Key : name of an image \n
        Value : grountruth associated (x, y coordinates)

    """
    
    dataset = json.load(json_file)
    
    dico_image_gt = dict()
    
    for i in range(len(dataset)):
        df = pd.DataFrame(columns=['x','y'])
        for j in range(len(dataset[i]['Label']['head'])):
            
            x = dataset[i]['Label']['head'][j]['geometry']['x']
            y = dataset[i]['Label']['head'][j]['geometry']['y']
            if x == 0 and y == 1939:
                break
            df.loc[j] = [x,y]
            
        dico_image_gt[dataset[i]['External ID']] = df
        
    return dico_image_gt
    