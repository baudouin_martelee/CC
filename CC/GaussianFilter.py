# -*- coding: utf-8 -*-
#pylint: disable=C0303,C0103
"""
Created on Wed Feb 19 15:21:13 2020

The code included in this module is borrowed from https://github.com/davideverona/deep-crowd-counting_crowdnet

This class is use only to apply a Gaussian filter on an image.
"""
import numpy as np
import scipy.spatial
import scipy.ndimage



def gaussian_filter_density(gt):
    """
    This method apply a Gaussian filter on a new array named density. 
    density have the same dimension of gt.
    This Gaussian filter is adaptative. 
    It means that the Gauss curve is changing depending the point it handle.
    So the sigma of the Gauss curve is changing if the neighbouring points are close or not.
    To access to this neighbouring points and take their distances between each others,
    the use of a KDTree is essentiel in term of computational cost.

    Parameters
    ----------
    gt : Array
        an binary array with coordinates of heads of people

    Returns
    -------
    Return : Array
        an array with the same dimension of gt where the gaussian filter is applied

    """
    print(gt.shape)
    density = np.zeros(gt.shape, dtype=np.float32)
    gt_count = np.count_nonzero(gt)
    if gt_count == 0:
        return density

    pts = np.array(list(zip(np.nonzero(gt)[1], np.nonzero(gt)[0])))
    leafsize = 2048
    # build kdtree
    tree = scipy.spatial.KDTree(pts.copy(), leafsize=leafsize)
    # query kdtree
    distances, locations = tree.query(pts, k=4) #4 per default

    print('generate density...')
    for i, pt in enumerate(pts):
        pt2d = np.zeros(gt.shape, dtype=np.float32)
        pt2d[pt[1], pt[0]] = 1.
        if gt_count > 1:
            sigma = (distances[i][1]+distances[i][2]+distances[i][3])*0.1
        else:
            sigma = np.average(np.array(gt.shape))/2./2. #case: 1 point
        density += scipy.ndimage.filters.gaussian_filter(pt2d, sigma, mode='constant')
    print('done.')
    return density
