# -*- coding: utf-8 -*-
#pylint: disable=C0303,C0103
"""
Created on Fri Mar 13 13:53:51 2020

@author: Baudouin


This module contains all the method related to the training of the model.
"""


import os, fnmatch, sys

import h5py

import torch
import torch.nn as nn
from torch.autograd import Variable
from torchvision import datasets, transforms

import time
from PIL import Image
from CC.model import CSRNet
import CC.dataset as dts
from CC.Importation import import_data
from CC.Image import getPaths, load_image_with_groundtruth, generate_density

def pytorch_loader(listpath_train, listpath_validation, dict_path_gt, batch_size):
    """
    Object to iterate over a dataset.
    We create 2 iterations. One on the trainset and an other on the validation set.
    We iterate using the class dataset 

    Parameters
    ----------
    listpath_train : List
        List containing paths of the training set
    listpath_validation : List
        List containing paths of the validation set
    dict_path_gt : Dictionnaire
        Key : Path of an image\n
        Value : Groundtruth density map of the associated image
    batch_size : Integer
        batch size

    Returns
    -------  
    Return : 
        train_loader : DataLoader
            Dataloader for the training set
        validation_loader : DataLoader
            Dataloader for the validation set

    """
    if not isinstance(listpath_train, list):
        raise listpath_train+"  is not a List"
    
    if not isinstance(listpath_validation, list):
        raise listpath_train+" is not a List"
        
    if not isinstance(dict_path_gt, dict):
        raise ""+dict_path_gt+" is not a dictionary"
    
    if not isinstance(batch_size, int):
        raise "batch size must be an integer"
    
    if batch_size <=0 :
        raise "batch size must be greater than 1"
    
    
    
    image_transform = transforms.Compose([
        transforms.Normalize(mean=[0.5, 0.5, 0.5],
                                     std=[0.5, 0.5, 
                                     0.5])
    ])

    
    
    ds_train = dts.ListDataset(listpath_train, dict_path_gt,image_transformation=image_transform)
    train_loader = torch.utils.data.DataLoader(dataset=ds_train, batch_size=batch_size)
    
    ds_val = dts.ListDataset(listpath_validation, dict_path_gt,image_transformation=image_transform)
    validation_loader = torch.utils.data.DataLoader(dataset=ds_val, batch_size=batch_size)
    
    
    return train_loader, validation_loader

def split_data(dict_img_path_gt, percentage):
    """
    This method split the training dataset into 2 sets : training set and validation set
    We must define a percentage. This percentage of the data set will be the training set.     

    Parameters
    ----------
    dict_img_path_gt : Dictionary
        Key : Path of an image\n
        Value : Groundtruth density map of the associated image
    threshold : Integer
        Number of percentage of the set that you want for the training set.

    Returns
    -------
    dico_train : Dictionary
        Key : Path of an image\n
        Value : Groundtruth density map of the associated image
    dico_validation : Dictionary
        Key : Path of an image\n
        Value : Groundtruth density map of the associated image

    """
    
    if not isinstance(dict_img_path_gt, dict):
        raise dict_img_path_gt+"is not a dictionary"
        
    if not isinstance(percentage, int):
        raise "percentage must be an integer"
    
    if percentage <=0 or percentage >100:
        raise " percentage must be between 1 and 100"
    

    print("Split du dico en train et validation")
    
    threshold = (len(dict_img_path_gt)* percentage)//100
    dico_train = dict(list(dict_img_path_gt.items())[:threshold])
    dico_validation = dict(list(dict_img_path_gt.items())[threshold:])
    
    return dico_train,dico_validation
    
    

def save_checkpoint(states, path):
    """
    
    To save the model that you runed.

    Parameters
    ----------
    states : TYPE
        The different state of the model. You can store the architecture, the nombre d'epoch, the optimizer, the learning rate.
        The important one is state_dict. State_dict contains the weights and biais.
    path : path
        Path to store your pth file.

    Returns
    -------
    Return : None.

    """
    torch.save(states, path)


#Path où le chemin du dossier où se trouve toute les données d'entrainements
        

def train(train_loader, validation_loader, num_epoch, learning_rate, pthFilePath):
    
    """
    Training of the model.
    We define a model and a mesur of loss (here MSELoss)
    
    For each epoch : 
        
        - for each image and associated groundtruth of the training set, we train the model and we change weights and biais doing backpropagation.
        - for each image and associated groundtruth of the validation set, we calcul the the mean absolute error to detect overfitting.
      
    we store the info of the model in a pth file (checkpoint file)
    

    Parameters
    ----------
    train_loader : DataLoader
            Dataloader for the training set
    validation_loader : DataLoader
            Dataloader for the validation set
    num_epoch : Integer
        Number of epochs
    learning_rate : Float
        Learning rate
    pthFilePath : Path
        The path where you want to store the pth file

    Returns
    -------
    None.

    """
    
    
    if num_epoch <= 0:
        raise " num epoch must be greater than 0"
    if learning_rate <=0:
        raise " learning rate must be positive"
        
    
    
    
    start = time.time()
    
    model = CSRNet().cuda()
    #Loss function
    criterion = torch.nn.MSELoss().cuda()
    
    #Optimizer
    optimizer = torch.optim.SGD(model.parameters(), lr=learning_rate, momentum=0.9,weight_decay=1e-6)
        
    print("Nombre total de params", sum(p.numel() for p in model.parameters()))
    
    loss_epoch = 0
    best_mae = 1000
    
    for epoch in range(num_epoch):
        print("Epoch n° {}".format(epoch))
        model.train()
        for i, (img, groundtrust_for_image) in enumerate(train_loader):  

            optimizer.zero_grad()
      
            img = img.cuda()
            img = Variable(img)
            
            out = model.forward(img)
      
            groundtrust_for_image =  groundtrust_for_image.type(torch.FloatTensor).unsqueeze(0).cuda()
            groundtrust_for_image = Variable(groundtrust_for_image)
            
            #print("Shape out : {}".format(out.shape))
            #print("Shape groundtruth : {}".format(groundtrust_for_image.shape))
            
            
            loss = criterion(out, groundtrust_for_image)
            loss.backward()
            #torch.nn.utils.clip_grad_norm_(model.parameters(), 0.5)
            optimizer.step()
            loss_epoch += loss.data
              
    
        print("Loss for epoch", epoch, ":", loss_epoch)
        mae = 0
        model.eval()
        with torch.no_grad():
            for i, (img, gt_for_image) in enumerate(validation_loader):
                img = img.cuda()
                img = Variable(img)
                output = model(img)
                
                mae += abs(output.data.sum()- gt_for_image.sum().type(torch.FloatTensor).cuda())
               
            mae = mae/len(validation_loader)
            print(' * MAE {}'.format(mae))
        best_mae = min(mae, best_mae)    
        print("Best mae : {}".format(best_mae))
        loss_epoch = 0

        save_checkpoint({
            'epoch': epoch + 1,
            'model': CSRNet(),
            'state_dict': model.state_dict(),
            'optimizer': optimizer.state_dict(),
        },pthFilePath)
    
    print("Temps d'execution : ", time.time()-start, 'secondes.')      
                  


def generate_density_map(list_img_paths,data):
    """
    This method takes a list of images.
    
    For each image, it creates a h5 file, if not already done, to store the density.
    We store this density in a dictionnary
    

    Parameters
    ----------
    list_img_paths : List
        List of paths of images
    data : Dictionary
        Key : Name of an image\n
        Value : Groundtruth coordinates of the associated  image

    Returns
    -------
    Return : Dictionary
        Key : Path of an image\n
        Value : Groundtruth density map of the associated image

    """
    if not isinstance(list_img_paths, list):
        raise " "+list_img_paths+"is not a list"
        
    if not isinstance(data, dict):
        raise " "+data+"is not a dictionary"
    
    
    print("Generation des densités et dictionnaire")
    dict_img_path_density = dict()

    for img_path in list_img_paths:

        name = img_path.split(".jpg")
        h5_path = name[0]+".h5" #name[0]
        if not os.path.exists(h5_path):
            Image.generate_density(img_path, data)
        with h5py.File(h5_path, 'r') as hf:
            dict_img_path_density[img_path] = hf['density'][()]
            
    return dict_img_path_density
            

def main():
    """
     

    Returns
    -------
    None.

    """


    print("Commencement projet")
    
    #SI exploitation sur spyder
    #root = '/home/baudouin/Documents/Dataset/ShanghaiTech'
    
    #Sur PC perso
    #root = 'C:/Users/Baudouin/Documents/IPL_BLOC_3/STAGE/'
    
    #Si exploitation sur notebook
    root = sys.argv[1]

    image_directory = os.path.join(root, 'Croix du Sud/train_data')
    #image_directory = os.path.join(root,'Croix du Sud')
    list_img_paths = Image.getPaths(image_directory)
    print(list_img_paths)
    
    
    #json du dataset
    json_file = open(os.path.join(image_directory, 'dataset_audit.json'))
    
    data = import_data(json_file)
    
    ##########################################
    #                                        #
    #   GENERATION DE LA GT MAP DENSITE      #
    #                                        #
    ##########################################
   
    dict_img_path_gt = generate_density_map(list_img_paths,data)
    
    ##########################################
    #                                        #
    #Split data into training and validation #
    #                                        #
    ##########################################
    dico_train, dico_validation = split_data(dict_img_path_gt, 80) 
    
    
    print("Génération terminée")

    
    
    # prendre une image 
    dico = dict_img_path_gt
    
    img,gt = Image.load_image_with_groundtruth(list(dict_img_path_gt.keys())[0], dico)
    #print(gt.shape[0])
    # visualisation de la premier image 
    #Visualisation.plot(img,gt)
    
    ##########################################
    #                                        #
    #   IMPORTATION DONNEES POUR PYTORCH     #
    #                                        #
    ##########################################
    print("Importation des données pour pytorch")
    
    train_loader, validation_loader = pytorch_loader(list(dico_train.keys()), list(dico_validation.keys()), dico, 1)


    
    ##########################################
    #                                        #
    #        Entrainement du modèle          #
    #                                        #
    ##########################################
    
    learning_rate = sys.argv[4]
    learning_rate = float(learning_rate)
    num_epoch = sys.argv[3]
    num_epoch = int(num_epoch)
    pth_file_path = sys.argv[2]
      
    print("Entrainement du modèle")
    train(train_loader, validation_loader, num_epoch=num_epoch, learning_rate=learning_rate, pthFilePath=pth_file_path)
    
    print("Entrainement fini !")
    
    

    
if __name__ == '__main__':
    main()        