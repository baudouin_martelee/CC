# -*- coding: utf-8 -*-
#pylint: disable=C0303,C0103
"""
Created on Wed Feb 19 15:21:13 2020

Visualisation

This class is use to plot image to have a better visualisation. 
This was very usefull for visualising the data augmentation, the predicted groudtruth and the original grountruth.
"""

import matplotlib.pyplot as plt
import numpy as np


def plot(img, gt):
    """
    Display an image and his associated groundtruth

    Parameters
    ----------
    img : PIL Image
        PIL image that you want to display
    gt : 2D array
        the groundtruth map density of the associated image.

    Returns
    -------
    Return : None.

    """
    f = plt.figure(figsize=(30, 10))
    ax1 = f.add_subplot(1, 4, 1)
    plt.imshow(img)
    ax1.title.set_text('Input Image')
    ax2 = f.add_subplot(1, 4, 2)
    plt.imshow(gt, cmap='jet')
    ax2.title.set_text('Ground-Truth Density map')
    plt.show(block=True)

    
    
def plotImage(img):
    """
    Display an image

    Parameters
    ----------
    img : PIL Image
        PIL image that you want to display

    Returns
    -------
    Return : None.

    """
    f = plt.figure(figsize=(30, 10))
    ax1 = f.add_subplot(1, 4, 1)
    plt.imshow(img)
    ax1.title.set_text('Input Image')
    plt.show(block=True)
    
