# -*- coding: utf-8 -*-
#pylint: disable=C0303,C0103
"""
Created on Mon Mar 30 16:07:06 2020

@author: Baudouin
"""
import random
import numpy as np
from torchvision import transforms
from torch.utils.data import Dataset
import torchvision.transforms.functional as TF
from CC.Image import load_image_with_groundtruth
from PIL import Image





class ListDataset(Dataset):
    """Class for importation of the dataset to pytorch"""
    
    def __init__(self, listpath, dictionnaire_path_gt=None, image_transformation=None, gt_transformation=None):
        """
        
        __init__ is the constructor of the class ListDataset.

        Parameters
        ----------
        listpath : List
            a list containing the different paths of images
        dictionnaire_path_gt : Dictionary
            a dictionnary where keys are paths of images  and values are groundtruth
        image_transformation : Transforms
            transformation for data augmentation to apply to the image
        gt_transformation : Transforms
            transformation for data augmentation to apply to the groundtruth
        
        Returns
        -------
        None.

        """
        self.paths = listpath #Problème  ici
        self.nombrePaths = len(self.paths)
        self.dictionnaire_path_gt = dictionnaire_path_gt
        self.image_transformation = image_transformation
        self.gt_transformation = gt_transformation

    
    def transform(self, image, groundtruth):
        """
        This is in this class that we apply data augmentation on images and associated groundtruth.
        You can change this class if you want to add, remove or change data augmentation.
        
        
        Parameters
        ----------
        image : PIL image 
            an image of auditorium
        groundtruth : array 
            groundtruth array

        Returns
        -------
        image : TYPE
            An augmented image of auditorium
        groundtruth : TYPE
            An augmented groundtruth array
        """
        
        transfo = transforms.ToPILImage()
        groundtruth = transfo(groundtruth)
        
        
        # Resize image
        #if random.random() >0.5:
        resize_img = transforms.Resize((224, 224))
        image = resize_img(image)

        

        resize_gt = transforms.Resize((28, 28))
        groundtruth = resize_gt(groundtruth)
        
        
        if random.random() > 0.7:
            image = TF.hflip(image)
            groundtruth = TF.hflip(groundtruth)
       
        # Random rotation
        if random.random() > 0.7:
             rotate_img = transforms.RandomAffine(3)
             image = rotate_img(image)
        

        

        groundtruth = np.array(groundtruth)

        #Visualisation.plot(image,groundtruth)

        # Transform to tensor
        image = TF.to_tensor(image)

       



        return image, groundtruth



    # renvoie la taille du dataset
    def __len__(self):
        """
        Get the number of paths of image from the directory 
        
        Returns
        -------
        The size of the dataset

        """
        return self.nombrePaths

    # renvoie l'image et le groundtruth correspondant à l'index "index"
    def __getitem__(self, index):
        """
        This method get the path of an image at index "index" of the list of paths.
        Then using Image.load_image_with groundtruth, we get the image and associated groundtruth from the path.
        To finish, we apply the method transform to make data augmentation.

        Parameters
        ----------
        index : Integer
            index of an image in the listpath containing image paths.

        Returns
        -------
        img : PIL Image
            An augmented image of auditorium
        gt : 2D array
            An augmented groundtruth array

        """
        
        img_path = self.paths[index]
        
        img, gt = load_image_with_groundtruth(img_path, self.dictionnaire_path_gt)
        
        #print("AvantPIL")
        #Visualisation.plot(img,gt)
   
        img, gt = self.transform(img, gt)

        if self.image_transformation is not None:
            img = self.image_transformation(img) 
        
      
        return img, gt


