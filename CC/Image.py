# -*- coding: utf-8 -*-
"""
Created on Feb 19 15:21:13 2020

@author: Baudouin

This module will contains all the method related to an image.
Either get information of an image or generate density.

"""

import h5py
import PIL.Image as Image
import os
import cv2
import glob
import numpy as np
import matplotlib.pyplot as plt
import scipy.io as io
import pandas as pd
import CC.GaussianFilter as gaussianfilter


def getPaths(path):
    """
    
    Get the paths of the images of a certain directory.


    Parameters
    ----------
    path : Path
        path of the directory containing images .jpg
    
    Returns
    -------
    Return : 
        Type : List\n
        Description : List containing the paths of each image of the directory 

    """
    
    img_paths = []
    
    for img_path in glob.glob(os.path.join(path,'*.jpg')):
        img_path = img_path.replace('\\', '/')
        img_paths.append(img_path)
        
    return img_paths
    



def load_image_with_groundtruth(img_path, dictionary_path_gt):
    """
    Get the paths of the images of a certain directory.

    Parameters
    ----------
    img_path : Path
        path of one image
        
    dictionary_path_gt: Dictionary
        Key : img_path\n
        Value : Groundtruth of the image
        
    Returns
    -------
    Return : List
        a list containing the paths of each image of the directory 

    """
    img = Image.open(img_path).convert('RGB')
    
    gt = dictionary_path_gt[img_path]

    gt= cv2.resize(gt,(int(gt.shape[1]/8), int(gt.shape[0]/8)), interpolation = cv2.INTER_CUBIC)*64
    
    
    return img, gt



def generate_density(img_path, dico_img_gt):
        """
        This method allows to generate density of an image from groundtruth and store this density in an h5 file.
        This create an array 
        
        

        Parameters
        ----------
        img_path :Path
            path of an image
        dico_img_gt : a dictionary
            key : path of an image\n
            value : groundtruth. an 2D array containing x and y coordinates of heads of people
    
        Returns
        -------
        Return : None.
    
        """
    
        #mat = io.loadmat(img_path.replace('images','ground-truth').replace('IMG_','GT_IMG_').replace('.jpg','.mat'))
        img_name = img_path.split("/")[-1]
        img= plt.imread(img_path)
        k = np.zeros((img.shape[0], img.shape[1]))
        gt = dico_img_gt[img_name]
        for i in range(0, len(gt)):
            if int(gt['y'][i])<img.shape[0] and int(gt['x'][i])<img.shape[1]:
                k[int(gt['y'][i]),int(gt['x'][i])]=1
        
        k = gaussianfilter.gaussian_filter_density(k)
        
        with h5py.File(img_path.replace('.jpg', '.h5'), 'w') as hf:
                hf.create_dataset('density', data=k)












