# -*- coding: utf-8 -*-
"""
Created on Tue Mar 31 16:54:18 2020
ok
@author: Baudouin


This module can handle a video to extract several frames from it. 
When these frames are extracted, we store them in a directory that we have to specify.
For each of this frame, we predict the number of people on it.


"""

import cv2 
import os 
import time
import threading
import math
import torch 
import torchvision.transforms.functional as F
from torchvision import transforms
import PIL.Image as Image
from CC.model import CSRNet
from torchvision import transforms

def takeShot(videoPath, capturesPath, checkpointPath,seconds=5):
    """
    Make shots of a video at every frame of this video

    Parameters
    ----------
    videoPath : Path
        Path of the video that you want to take shots
    capturesPath : Path
        Path of the directory where you want to store your shots
    checkpointPath : Path
        Path of the pth file to create a new model for predictions

    Returns
    -------
    Display the shots and the predictions for each shot

    """
    
    if not os.path.isfile(videoPath):
        raise AttributeError("Incorrect path of the video")
    
    
    if not os.path.isfile(checkpointPath):
        raise AttributeError("Incorrect path of the pth file (checkpoint)")
    
    cap = cv2.VideoCapture(videoPath)
    frameRate = cap.get(cv2.CAP_PROP_FPS) # get the frame rate of the video

    print("frame rate = {}".format(frameRate))
    x=1
    
    model = CSRNet()
    
    print("debut")
    if os.path.isfile(checkpointPath):
        checkpoint = torch.load(checkpointPath, map_location={'cuda:0': 'cpu'})
        model.load_state_dict(checkpoint['state_dict'])
        print("model loaded")
    
    count=0
    
    while(cap.isOpened()):
        ret, frame = cap.read()
        
        if (ret != True):
            cap.release()
            break
        else:
            filename = capturesPath + 'capture' +  str(int(x)) + ".jpg";x+=1
            print(filename)
            cv2.imwrite(filename, frame)
            count += (frameRate *seconds) # toute les 5 secondes
            cap.set(cv2.CAP_PROP_POS_FRAMES, count) # next id of the frame to take
            print("current position = {} second".format(cap.get(cv2.CAP_PROP_POS_MSEC )/1000)) 
            
            
        
        img = Image.open(filename).convert('RGB')
        #img.show()
        resize_img = transforms.Resize((224, 224))
        img = resize_img(img)
        
        img = F.to_tensor(img)
        normalizeImage = transforms.Normalize(mean=[0.5, 0.5, 0.5],
                                     std=[0.5, 0.5, 
                                     0.5])

        img = normalizeImage(img)
        
        output = model(img.unsqueeze(0))
        print("Predicted Count : ",int(output.detach().cpu().sum().numpy()))
       
        
    
    cv2.destroyAllWindows()
    print ("Done!")
    
if __name__ == '__main__':
    
    videoPath = r"C:/Users/Baudouin/Documents/IPL_BLOC_3/STAGE/VIDEO/people count SC13B/VMSExport02.avi"
    checkpointPath = 'C:/Users/Baudouin/Documents/IPL_BLOC_3/STAGE/CrowdCounting/test_100epoch_lr5.pth.tar'
    capturesPath = 'C:/Users/Baudouin/Documents/IPL_BLOC_3/STAGE/Capture video/video 4/'
    seconds = 5
    
    takeShot(videoPath, capturesPath ,checkpointPath,seconds)    
