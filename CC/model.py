# -*- coding: utf-8 -*-
#pylint: disable=C0303,C0103
"""
Model Class 


Here we define the model. This is our architecture for the neural network.
It is split in two part : the frontend and the back end.
the two parts are concatened to form the model.

The frontend follow the VGG-16 architecture to facilitate transfer learning
the backend use dilated convolution for segmentation tasks.

We initialize weight of the model with value from a normal distribution.
"""

import torch.nn as nn
from torchvision import models

class CSRNet(nn.Module):
    """  CSRNet Model """
    
    
    def __init__(self, load_weights=False):
        """
        We initialize the frontend with the 10 first layers of convolution and the 3 first layers of max-pooling of VGG-16
        We initialise frontend with 6 convolutions layers 
        we initalise the output layers 
        We download the pretrained network for the frontend and store it in a state_dict
        
        Parameters
        ----------
        load_weights : pth.tar file
            A pth.tar file who contains the weights and biais of the model. its False per default because we use the pretrained model of vgg-16.
            But you can put your own weights

        Returns
        -------
        None.

        """
        super(CSRNet, self).__init__()
        self.frontend_feat = [64, 64, 'M', 128, 128, 'M', 256, 256, 256, 'M', 512, 512, 512]
        self.backend_feat = [512, 512, 512, 256, 128, 64]
        self.frontend = make_layers(self.frontend_feat)
        self.backend = make_layers(self.backend_feat, in_channels=512, dilation=True)
        self.output_layer = nn.Conv2d(64, 1, kernel_size=1)
        if not load_weights:
            mod = models.vgg16(pretrained=True)
            self.initialize_weights()
            for i in range(len(self.frontend.state_dict().items())):
                list(self.frontend.state_dict().items())[i][1].data[:] = list(mod.state_dict().items())[i][1].data[:]
                
    def forward(self, x):
        """
        The forward propagation

        Parameters
        ----------
        x : Tensor
            tensor before all the calculation of the modele

        Returns
        -------
        x : Tensor
            tensor after all the calculation of the modele

        """
        x = self.frontend(x)
        x = self.backend(x)
        x = self.output_layer(x)
        
        return x
    
    def initialize_weights(self):
        """
        We initialise randomly the weights with values that follow a normal distribution
        

        Returns
        -------
        None.

        """
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.normal_(m.weight, std=0.01)
                if m.bias is not None:
                    nn.init.constant_(m.bias, 0)
                
def make_layers(list_part_model, in_channels=3, dilation=False):
    """
    Method that create layers for each part of the model.

    Parameters
    ----------
    list_part_model : List
        List of numbers of channel
    in_channels : Integer
        number of channel as input. The default is 3.
    batch_norm : TYPE, optional
        DESCRIPTION. The default is False.
    dilation : Integer
        Dilatation of the kernel

    Returns
    -------
    A sequential container. 
        Modules will be added to it in the order they are passed in the constructor.
        
    """
    if dilation:
        d_rate = 2
    else:
        d_rate = 1
    layers = []
    for v in list_part_model:
        if v == 'M':
            layers += [nn.MaxPool2d(kernel_size=2, stride=2)]
        else:
            conv2d = nn.Conv2d(in_channels, v, kernel_size=3, padding=d_rate, dilation=d_rate)
            layers += [conv2d, nn.ReLU(inplace=True)]
            in_channels = v
    return nn.Sequential(*layers)
